'use strict'
/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2
function indexOf(arr, item) {

}
*/
function indexOf(arr, item) {
    if (Array.prototype.indexOf) {
        return arr.indexOf(item);
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) {
                return i;
            }
        }
    }
    return -1;
}
console.log(indexOf([1, 2, 3, 4], 3));
/*
2、题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10
function sum(arr) {

}
*/

function sum(arr) {
    var add = 0;
    for (var i = 0; i < arr.length; i++) {
        add += arr[i];
    }
    return add;
}
console.log(sum([1, 2, 3, 4]));


/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]

function remove(arr, item) {

}
*/

function remove(arr, item) {
    var a = arr.slice(0);
    for (var i = 0; i < arr.length; i++) {
        if (a[i] == item) {
            a.splice(i, 1);
        }
    }
    return a;
}
console.log(remove([1, 2, 3, 4, 2], 2));
/*


4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]

function removeWithoutCopy(arr, item) {

}
*/
function removeWithoutCopy(arr, item) {
    var newarr = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] != item) {
            newarr.push(arr[i]);
        }
    }
    return newarr;
}
console.log(removeWithoutCopy([1, 2, 2, 3, 4, 2, 2], 2));


/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
function append(arr, item) {
    return arr.concat(item);
}
console.log(append([1, 2, 3, 4], 10));

/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function truncate(arr) {

}
*/
function truncate(arr) {
    return arr.slice(0, arr.length - 1)
}
console.log(truncate([1, 2, 3, 4]));

/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/
function prepend(arr, item) {
    return [item].concat(arr);
}
console.log(prepend([1, 2, 3, 4], 10));

/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/
function curtail(arr) {
    var newarr = arr.slice(0);
    newarr.shift();
    return newarr;
}
console.log(curtail([1, 2, 3, 4]));

/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
function concat(arr1, arr2) {
    var arr = arr1.slice(0);
    arr = arr.concat(arr2);
    return arr;
}
console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]));

/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/
function insert(arr, item, index) {
    var newarr = arr.slice(0);
    newarr.splice(index, 0, item);
    return newarr;
}
console.log(insert([1, 2, 3, 4], 'z', 2));

/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/
function count(arr, item) {
    var num = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == item) {
            num++;
        }
    }
    return num;
}
console.log(count([1, 2, 4, 4, 3, 4, 3], 4));
/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/
function duplicates(arr) {
    var newarr = [];
    arr.forEach(function (x) {
        if (arr.indexOf(x) != arr.lastIndexOf(x) && newarr.indexOf(x) == -1)
        newarr.push(x);
    })
    return newarr;
}
console.log(duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]));

/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
function square(arr) {
    return arr.map(function (x) {
        return x * x;
    })
}
console.log(square([1, 2, 3, 4]));
/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/
function findAllOccurrences(arr, target) {
    var newarr = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === target) {
            newarr.push(arr.indexOf(arr[i], i));
            i++;
        }
    }
    return newarr;
}
console.log(findAllOccurrences(['a', 'b', 'c', 'd', 'e', 'f', 'a', 'b', 'c'], 'a'));

/*
15、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/
function identity(val1, val2) {
    if (val1 === val2) {
        return true;
    } else {
        return false;
    }
}
console.log(identity(111, 111));
/*
16、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
function area_of_circle(r, pi) {
    return r * r * pi;
}
console.log(area_of_circle(2, 3.14));

/*
17、题目描述

<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>

针对以上文档结构，请把与Web开发技术不相关的节点删掉：

注意！！请分别使用原生JS和jQuery两种试实现！！！！！

*/


/*
18、题目描述

对如下的Form表单：

<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
		<p><button type="submit">Submit</button></p>
    </fieldset>
</form>

要求:
绑定合适的事件处理函数，实现以下逻辑：

当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

当用户去掉“全不选”时，自动不选中所有语言；

当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。


*/
var
    form = $('#test-form'),
    langs = form.find('[name=lang]'),
    selectAll = form.find('label.selectAll :checkbox'),
    selectAllLabel = form.find('label.selectAll span.selectAll'),
    deselectAllLabel = form.find('label.selectAll span.deselectAll'),
    invertSelect = form.find('a.invertSelect');

// 重置初始化状态:
form.find('*').show().off();
form.find(':checkbox').prop('checked', false).off();
deselectAllLabel.hide();
// 拦截form提交事件:
form.off().submit(function (e) {
    e.preventDefault();
    alert(form.serialize());
});




$(function () {
    // 全选
    selectAll.change(function () {
        if (selectAll.is(':checked')) {
            langs.prop('checked', true);
            selectAllLabel.hide();
            deselectAllLabel.show();
        } else {
            langs.prop('checked', false);
            deselectAllLabel.hide();
            selectAllLabel.show();
        }
    });

    langs.change(function () {
        if (isAllChecked()) {
            selectAllLabel.hide();
            selectAll.prop('checked', true);
            deselectAllLabel.show();
        } else {
            selectAllLabel.show();
            selectAll.prop('checked', false);
            deselectAllLabel.hide();
        }
    });

    // 反选
    invertSelect.click(function () {
        langs.map(function () {
            $(this).prop('checked', !($(this).prop('checked')));
        });
        langs.change();
    });

    function isAllChecked() {
        var flag = true;
        langs.filter(function () {
            if (!$(this).is(':checked')) {
                flag = false;
            }
        });
        return flag;
    }
});